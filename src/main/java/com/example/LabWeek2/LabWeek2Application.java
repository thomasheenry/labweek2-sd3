package com.example.LabWeek2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabWeek2Application {

	public static void main(String[] args) {
		SpringApplication.run(LabWeek2Application.class, args);
	}

}
