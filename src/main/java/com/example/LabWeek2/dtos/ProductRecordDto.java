package com.example.LabWeek2.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record ProductRecordDto(@NotBlank String name, @NotNull BigDecimal value, @NotBlank String description,
                               @NotBlank String quantityPerStock) {
}
